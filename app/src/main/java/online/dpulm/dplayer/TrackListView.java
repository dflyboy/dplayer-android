package online.dpulm.dplayer;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import online.dpulm.dplayer.Audio.Track;

public class TrackListView extends ViewModel {
    private MutableLiveData<List<Track>> users;
    public LiveData<List<Track>> getUsers() {
        if (users == null) {
            users = new MutableLiveData<List<Track>>();
            loadUsers();
        }
        return users;
    }

    private void loadUsers() {
        // Do an asynchronous operation to fetch users.
    }
}
