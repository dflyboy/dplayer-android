package online.dpulm.dplayer;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import online.dpulm.dplayer.Audio.Album;
import online.dpulm.dplayer.Audio.Track;
import online.dpulm.dplayer.ViewModels.AlbumViewModel;
import pub.devrel.easypermissions.EasyPermissions;

import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import static online.dpulm.dplayer.AlbumLoadService.ACTION_LOAD;

public class AlbumActivity extends AppCompatActivity implements AlbumLoader.CallBack, TracklistFragment.OnListFragmentInteractionListener, EasyPermissions.PermissionCallbacks {

    private Album album;
    private Toolbar toolbar;
    private ProgressBar pBar;
    private TextView statusText;
    private static final String TAG = AlbumActivity.class.getSimpleName();
    private static final int WRITE_REQUEST_CODE = 300;

    private AlbumViewModel model;
    private AlbumLoadService mService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        statusText = findViewById(R.id.statusText);

        Intent intent = getIntent();
        int aId = ((Intent) intent).getIntExtra(App.ALBUM_ID, 0);
        model = ViewModelProviders.of(this).get(AlbumViewModel.class);
        model.loadAlbum(aId);
        model.getAlbum().observe(this, item -> {
            album = item;
            fillInfo();
        });
//        fetchAlbum(aId);
        bind();

        pBar = findViewById(R.id.progressBar3);
        final AlbumLoader.CallBack cb = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                if (isExternalStorageWritable()) {
                    if (EasyPermissions.hasPermissions(AlbumActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        loadService(album.get_id());

                        Snackbar.make(view, "Downloading " + album.getName(), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
//                        new AlbumLoader(cb).execute(url);
                    } else {
                        EasyPermissions.requestPermissions(AlbumActivity.this, "Need perms", WRITE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                } else {
                    Snackbar.make(view, "Not writeable!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        bind();
    }

    private void bind() {
        Intent intent = new Intent(this, AlbumLoadService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void loadService(int id) {
        Intent intent = new Intent(this, AlbumLoadService.class);
        intent.putExtra(AlbumLoadService.ALBUM_ID, id);
        intent.setAction(ACTION_LOAD);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AlbumLoadService.LocalBinder binder = (AlbumLoadService.LocalBinder) service;
            mService = binder.getService();
            mService.setCallback(AlbumActivity.this);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, AlbumActivity.this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        //Download the file once permission is granted
        loadService(album.get_id());

        Snackbar.make(findViewById(R.id.app_bar), "Downloading " + album.getName(), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "Permission has been denied");
    }

    protected void fillInfo() {
        toolbar.setTitle(album.getName());
        setSupportActionBar(toolbar);

        ImageView cover = findViewById(R.id.coverView);
        cover.setImageBitmap(album.getCover());
        TextView text = findViewById(R.id.infoText);
        text.setText(album.getArtist().getName());

        String info = "";
        if (album.getTrackN() > 0) {
            info += album.getTrackN() + " Tracks";
        }
        if (album.getDiscN() > 0) {
            info += "\n" + album.getDiscN() + " Discs";
        }
        TextView small = findViewById(R.id.smallText);
        small.setText(info);

    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void setProgressPercent(int percent, String step) {
        pBar.setProgress(percent, true);
        statusText.setVisibility(View.VISIBLE);
        statusText.setText(step);
    }

    @Override
    public void onFinished(String res) {

        Snackbar.make(toolbar, "Done!" + res, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        statusText.setVisibility(View.INVISIBLE);

        if( mBound) {
            unbindService(mConnection);
            mBound = false;
        }

    }

    @Override
    public void onListFragmentInteraction(Track item) {
//        item.play();

        Intent intent = new Intent(App.getAppContext(), PlayerService.class);
        intent.setAction(PlayerService.ACTION_PLAY);
        intent.putExtra(PlayerService.TRACK_ID, item.get_id());
        startService(intent);

//        String url = item.getUrl();
//        mMediaPlayer = new MediaPlayer();
//        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        try {
//            mMediaPlayer.setDataSource(url);
//            mMediaPlayer.setOnPreparedListener(this);
//            mMediaPlayer.prepareAsync(); // might take long! (for buffering, etc)
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

}
