package online.dpulm.dplayer;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import online.dpulm.dplayer.Audio.Album;

import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;

public class AlbumListActivity extends AppCompatActivity implements AlbumItemFragment.OnListFragmentInteractionListener {

    public String getQuery() {
        return query;
    }

    public void hideProgress() {
        ProgressBar p = findViewById(R.id.progressBar);
        p.setVisibility(View.GONE);
    }
    private String query = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        query = ((Intent) intent).getStringExtra(App.SEARCH_QUERY);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Albums");
        setSupportActionBar(toolbar);

    }

    @Override
    public void onListFragmentInteraction(Album item) {
        Intent intent = new Intent(this, AlbumActivity.class);
        intent.putExtra(App.ALBUM_ID, item.get_id());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_album_list, menu);
        return true;
    }


}
