package online.dpulm.dplayer;

import android.Manifest;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import pub.devrel.easypermissions.EasyPermissions;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.text.format.DateUtils.getRelativeTimeSpanString;

public class MainActivity extends AppCompatActivity implements IndexUpdater.Callback {
    private EditText edit;
    private static final int WRITE_REQUEST_CODE = 300;
    private static final String TAG = "MainActivity";
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        startIndexTimer();

        Log.d(TAG, Config.baseUrl);
        edit = findViewById(R.id.editText);
        edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    startSearch(v);
                    return true;
                }
                return false;            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }

    private void startIndexTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new IndexUpdater(MainActivity.this).execute(false);
                    }
                });
            }
        }, 1000, 5000);
    }

    public void showAlbums(View view) {

        if (hasStoragePerm()) {
            Intent intent = new Intent(this, AlbumListActivity.class);
            startActivity(intent);
        }
    }

    public boolean startIndexing(MenuItem view) {
        IndexUpdater indexUpdater = new IndexUpdater(MainActivity.this);
        indexUpdater.execute(true);
        return true;
    }

    public boolean hasStoragePerm() {
        if (EasyPermissions.hasPermissions(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(MainActivity.this, "Need perms", WRITE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return false;
        }
    }

    public void startSearch(View view) {
        if (hasStoragePerm()) {
//        Intent intent = new Intent(this, ResultActivity.class);
            Intent intent = new Intent(this, AlbumListActivity.class);
            String query = edit.getText().toString();
            intent.putExtra(App.SEARCH_QUERY, query);
            startActivity(intent);
        }
    }

    public void openAlbum(View view) {
        Intent intent = new Intent(this, AlbumActivity.class);
        EditText edit = findViewById(R.id.editAlbumId);
        int id = Integer.parseInt(edit.getText().toString());
        intent.putExtra(App.ALBUM_ID, id);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(IndexUpdater.IndexResponse response) {

        CardView indexCard = findViewById(R.id.indexCard);
        indexCard.setVisibility(response.isRunning() ? View.VISIBLE : View.GONE);

        if(response.isRunning()) {
            TextView indexText = findViewById(R.id.textViewIndex);
            indexText.setText( R.string.i_running );
            TextView started = findViewById(R.id.textViewStarted);
            started.setText(getRelativeTimeSpanString(response.getStarted(), new Date().getTime(), 0));
            TextView items = findViewById(R.id.textViewItems);
            items.setText(response.getRemaining() > 0 ? String.valueOf(response.getRemaining()) : "unknown");
        } else {
            TextView indexText = findViewById(R.id.textViewIndex);
            indexText.setText( R.string.i_not_running );
        }

    }

}

