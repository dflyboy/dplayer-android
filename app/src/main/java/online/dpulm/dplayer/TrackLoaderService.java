package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import online.dpulm.dplayer.Audio.Album;
import online.dpulm.dplayer.Audio.Track;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class TrackLoaderService extends IntentService implements TrackLoader.CallBack {
    private static final String TAG = "TrackLoader";

    public static String TRACK_ID = "online.dpulm.dplayer.TRACK_ID";
    private Track track;

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private DownloadManager.Query query;
    private Uri downloaded;
    private long dl;

    private final String CHANNEL_ID = "c_default";
    private final int notificationId = 419;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager notificationManager;
    private String tracks = "";

    private boolean running = false;
    private int progress = 0;
    private AlbumLoader.CallBack mCallback = null;

    public TrackLoaderService() {
        super("TrackLoaderService");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
        super.onCreate();
        downloadManager = App.getAppContext().getSystemService(DownloadManager.class);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int id = intent.getIntExtra(TRACK_ID, -1);
        fetchTrack(id);
    }

    public void fetchTrack(final int id) {
        String url = Config.trackUrl + "/" + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        track = gson.fromJson(response, Track.class);
                        loadTrack();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO: err handler
            }
        });

        // Add the request to the RequestQueue.
        App.getRequestQueue().add(stringRequest);
    }

    private void loadTrack() {
        createNotificationChannel();
        createNote();
        String url = Config.trackUrl + "/" + track.get_id() + "/stream";
        new TrackLoader(this).execute(url, track.getFile());

    }

    private void createNote() {
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_cloud_download_black_24dp)
                .setContentText("Waiting for backend")
                .setContentTitle(track.getName())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOngoing(true)
                .setOnlyAlertOnce(true);

        mBuilder.setProgress(100, 0, false);

        Intent albumIntent = new Intent(this, AlbumActivity.class);
        albumIntent.putExtra(App.ALBUM_ID, track.get_id());
        PendingIntent pendingIntentA = PendingIntent.getActivity(App.getAppContext(), track.get_id(), albumIntent, FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(pendingIntentA);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId, mBuilder.build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void setProgressPercent(int percent, String step) {
        progress = percent;
        mBuilder.setContentText(step)
                .setProgress(100, percent, percent == 100);
        notificationManager.notify(notificationId, mBuilder.build());
        if(mCallback != null) {
            mCallback.setProgressPercent(percent, step);
        }
    }

    @Override
    public void onFinished(String msg) {
        mBuilder.setContentText("Complete")
                .setProgress(0,0,false)
                .setOngoing(false)
                .setAutoCancel(true);
        notificationManager.notify(notificationId, mBuilder.build());
        running = false;
        tracks = msg;
        if(mCallback != null) {
            mCallback.onFinished(msg);
        }
        stopSelf();
    }

}
