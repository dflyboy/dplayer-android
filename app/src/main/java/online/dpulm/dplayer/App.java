package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import online.dpulm.dplayer.Audio.Album;

public class App extends Application {
    public static final String SEARCH_QUERY = "online.dpulm.dPlayer.QUERY";
    public static final String ALBUM_ID = "online.dpulm.dPlayer.ALBUMID";

    private static Context context;
    private static RequestQueue mRequestQueue;
    private static ContentResolver cr;
    private static Uri mediaStore;
    private static List<Album> albums;

    public static ImageLoader getImageLoader() {
        return mImageLoader;
    }

    private static ImageLoader mImageLoader;

    public static RequestQueue getRequestQueue() {
        return App.mRequestQueue;
    }


    @TargetApi(Build.VERSION_CODES.O)
    public static boolean hasAlbum(String album) {
        List<String> albums = new ArrayList<>();

        String[] mProjection =
                {
                        MediaStore.Audio.AlbumColumns.ALBUM,
                };

        String mSelectionClause = null;
        String[] mSelectionArgs = {""};

        mSelectionClause = MediaStore.Audio.AlbumColumns.ALBUM + " = ?";
        ;
        mSelectionArgs[0] = album;

//        Cursor mCursor = cr.query(mediaStore, mProjection, null, null);

        Cursor mCursor = cr.query(
                mediaStore,  // The content URI of the words table
                mProjection,                       // The columns to return for each row
                mSelectionClause,                  // Either null, or the word the user entered
                mSelectionArgs,                    // Either empty, or the string the user entered
                null);                       // The sort order for the returned rows

        return (mCursor.getCount() > 0);
    }

    public void onCreate() {
        super.onCreate();
        App.context = getApplicationContext();
        App.mRequestQueue = Volley.newRequestQueue(App.context);


        cr = context.getContentResolver();
        mediaStore = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        App.mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {

            private final int cacheSize = 64 * 1024 * 1024;
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(cacheSize);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    public static Context getAppContext() {
        return App.context;
    }
}
