package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;

public class IndexUpdater extends AsyncTask<Boolean, Void, Void> {

    private IndexUpdater.Callback cb;
    private ArrayList<String> files;

    @TargetApi(Build.VERSION_CODES.M)
    public IndexUpdater(IndexUpdater.Callback callBack) {
        super();
        cb = callBack;
    }

    @Override
    protected Void doInBackground(Boolean... params) {
        boolean request = params[0];
        fetchData(request);
        return null;
    }

    public interface Callback {
        public void update(IndexResponse response);
    }

    public class IndexResponse {
        private boolean running;
        private long started;
        private int remaining;

        public boolean isRunning() {
            return running;
        }

        public long getStarted() {
            return started;
        }

        public int getRemaining() {
            return remaining;
        }
    }

    private void fetchData(boolean requestIndex) {
        String url = Config.indexUrl + (requestIndex ? "" : "/stats");

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    IndexResponse indexResponse = gson.fromJson(response, IndexResponse.class);

                    cb.update(indexResponse);
                }, error -> {
                    //TODO: err handler
                });

        // Add the request to the RequestQueue.
        App.getRequestQueue().add(stringRequest);
    }

}
