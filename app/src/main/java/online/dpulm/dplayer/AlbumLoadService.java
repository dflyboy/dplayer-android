package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import online.dpulm.dplayer.Audio.Album;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class AlbumLoadService extends IntentService implements AlbumLoader.CallBack {

    public static String ALBUM_URL = "online.dpulm.dplayer.ALBUM_URL";
    public static String ALBUM_ID = "online.dpulm.dplayer.ALBUM_ID";

    public static final String ACTION_LOAD = "online.dpulm.dplayer.action.DOWNLOAD";

    private Album album;
    private final String CHANNEL_ID = "c_default";
    private final int notificationId = 419;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager notificationManager;
    private final IBinder mBinder = new LocalBinder();
    private String tracks = "";

    private boolean running = false;
    private int progress = 0;
    private AlbumLoader.CallBack mCallback = null;

    public int getProgress() {
        return progress;
    }


    public String getTracks() {
        return tracks;
    }

    public void setCallback(AlbumLoader.CallBack mCallback) {
        this.mCallback = mCallback;
    }

    public AlbumLoadService() {
        super("AlbumLoadService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void loadAlbum(final int id) {
        String url = Config.albumUrl + "/" + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        final Album rawAlbum = gson.fromJson(response, Album.class);

                        ImageRequest imgRequest = new ImageRequest(rawAlbum.getCoverUrl(),
                                new Response.Listener<Bitmap>() {
                                    @TargetApi(Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onResponse(Bitmap bitmap) {
//                                        BitmapDrawable bg = new BitmapDrawable(bitmap);
                                        rawAlbum.setCover(bitmap);
                                        album = rawAlbum;
                                        start();
                                    }
                                }, 0, 0, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.RGB_565,
                                new Response.ErrorListener() {
                                    public void onErrorResponse(VolleyError error) {
                                        //TODO: err handler
                                    }
                                });
                        App.getRequestQueue().add(imgRequest);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO: err handler
            }
        });

        // Add the request to the RequestQueue.
        App.getRequestQueue().add(stringRequest);
    }

    private void start() {
        String url = (Config.albumUrl + "/" + album.get_id() + "/download");
        new AlbumLoader(this).execute(url);
        createNotificationChannel();
        createNote();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(!running && intent.getAction().equals(ACTION_LOAD)) {
            running = true;
            int id = intent.getIntExtra(ALBUM_ID, -1);

            loadAlbum(id);
        }
    }

    public class LocalBinder extends Binder {
        AlbumLoadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return AlbumLoadService.this;
        }
    }

    private void createNote() {
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_cloud_download_black_24dp)
                .setContentText("Waiting for backend")
                .setContentTitle(album.getName())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOngoing(true)
                .setLargeIcon(album.getCover())
                .setOnlyAlertOnce(true);

        mBuilder.setProgress(100, 0, false);

        Intent albumIntent = new Intent(this, AlbumActivity.class);
        albumIntent.putExtra(App.ALBUM_ID, album.get_id());
        PendingIntent pendingIntentA = PendingIntent.getActivity(App.getAppContext(), album.get_id(), albumIntent, FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(pendingIntentA);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId, mBuilder.build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void setProgressPercent(int percent, String step) {
        progress = percent;
        mBuilder.setContentText(step)
                .setProgress(100, percent, percent == 100);
        notificationManager.notify(notificationId, mBuilder.build());
        if(mCallback != null) {
            mCallback.setProgressPercent(percent, step);
        }
    }

    @Override
    public void onFinished(String msg) {
        mBuilder.setContentText("Complete")
                .setProgress(0,0,false)
                .setOngoing(false)
        .setAutoCancel(true);
        notificationManager.notify(notificationId, mBuilder.build());
        running = false;
        tracks = msg;
        if(mCallback != null) {
            mCallback.onFinished(msg);
        }
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
