package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.io.IOException;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import online.dpulm.dplayer.Audio.Track;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class PlayerService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener {

    public static final String ACTION_PLAY = "online.dpulm.dplayer.action.PLAY";
    public static final String ACTION_PAUSE = "online.dpulm.dplayer.action.PAUSE";
    public static final String ACTION_STOP = "online.dpulm.dplayer.action.STOP";

    public static final String TRACK_ID = "online.dpulm.dplayer.Track.ID";
    private final IBinder mBinder = new LocalBinder();

    private final String CHANNEL_ID = "c_player";
    private final int notificationId = 418;
    private NotificationCompat.Builder mBuilder;
    private Track track;

    MediaPlayer mMediaPlayer = null;
    AudioAttributes mAudioAttributes = null;
    AudioManager mAudioManager;
    AudioFocusRequest mFocusRequest;
    private boolean mPlaybackDelayed, mResumeOnFocusGain = false;
    final Object mFocusLock = new Object();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stop();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                if (mPlaybackDelayed || mResumeOnFocusGain) {
                    synchronized (mFocusLock) {
                        mPlaybackDelayed = false;
                        mResumeOnFocusGain = false;
                    }
                    play();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                synchronized (mFocusLock) {
                    // this is not a transient loss, we shouldn't automatically resume for now
                    mResumeOnFocusGain = false;
                    mPlaybackDelayed = false;
                }
                pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // we handle all transient losses the same way because we never duck audio books
                synchronized (mFocusLock) {
                    // we should only resume if playback was interrupted
                    mResumeOnFocusGain = mMediaPlayer.isPlaying();
                    mPlaybackDelayed = false;
                }
                pause();
                break;
        }
    }


    public class LocalBinder extends Binder {
        PlayerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return PlayerService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
        }
        stopForeground(true);

    }

    private void createNote(boolean running) {
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_play_arrow_black_24dp)
                .setContentText(track.getArtist().getName())
                .setContentTitle(track.getName())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(true)
                .setSound(null)
                .setLargeIcon(track.getCover());

        addNoteAction(running);

        Intent albumIntent = new Intent(this, AlbumActivity.class);
        albumIntent.putExtra(App.ALBUM_ID, track.getAlbum().get_id());
        PendingIntent pendingIntentA = PendingIntent.getActivity(App.getAppContext(), track.getAlbum().get_id(), albumIntent, FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(pendingIntentA);

        // notificationId is a unique int for each notification that you must define
//        notificationManager.notify(notificationId, mBuilder.build());
        startForeground(notificationId, mBuilder.build());
    }

    private void addNoteAction(boolean running) {
        mBuilder.mActions.clear();

        Intent pauseIntent = new Intent(this, PlayerService.class);
        pauseIntent.setAction(ACTION_PAUSE);

        PendingIntent pendingIntentP = PendingIntent.getService(App.getAppContext(), track.get_id(), pauseIntent, FLAG_CANCEL_CURRENT);

        mBuilder.addAction(running ? R.drawable.ic_pause_black_24dp : R.drawable.ic_play_arrow_black_24dp, "Pause", pendingIntentP);

        Intent stopIntent = new Intent(this, PlayerService.class);
        stopIntent.setAction(ACTION_STOP);

        PendingIntent stopPending = PendingIntent.getService(App.getAppContext(), track.get_id(), stopIntent, FLAG_CANCEL_CURRENT);
        mBuilder.addAction(R.drawable.ic_stop_black_24dp, "Stop", stopPending);

        androidx.media.app.NotificationCompat.MediaStyle mediaStyle = new androidx.media.app.NotificationCompat.MediaStyle();
        mediaStyle.setCancelButtonIntent(stopPending);
        mediaStyle.setShowCancelButton(true);
        mediaStyle.setShowActionsInCompactView(0, 1);
        mBuilder.setStyle(mediaStyle);
    }

    private void startPlayback() {
        // initialization of the audio attributes and focus request
        mAudioManager = (AudioManager) App.getAppContext().getSystemService(Context.AUDIO_SERVICE);
        mFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(mAudioAttributes)
                .setAcceptsDelayedFocusGain(true)
                .setWillPauseWhenDucked(true)
                .setOnAudioFocusChangeListener(this)
                .build();

        mPlaybackDelayed = false;

        // requesting audio focus
        int res = mAudioManager.requestAudioFocus(mFocusRequest);
        synchronized (mFocusLock) {
            if (res == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
                mPlaybackDelayed = false;
            } else if (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                mPlaybackDelayed = false;
                play();
            } else if (res == AudioManager.AUDIOFOCUS_REQUEST_DELAYED) {
                mPlaybackDelayed = true;
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(ACTION_PLAY)) {
            int id = intent.getIntExtra(TRACK_ID, -1);
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
            }
            mAudioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setAudioAttributes(mAudioAttributes);
            mMediaPlayer.setOnCompletionListener(this);

            mMediaPlayer.setOnPreparedListener(this);
            fetchTrack(id);

        } else if (intent.getAction().equals(ACTION_PAUSE)) {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying()) {
                    mAudioManager.abandonAudioFocusRequest(mFocusRequest);
                    pause();
                } else {
                    startPlayback();
                }
            }

        } else if (intent.getAction().equals(ACTION_STOP)) {
            stop();
        }
        return Service.START_STICKY;
    }

    private void play() {
        if (mMediaPlayer != null) {
            createNote(true);
            mMediaPlayer.start();
        }
    }

    private void pause() {
        if (mMediaPlayer != null) {
            createNote(false);
            mMediaPlayer.pause();
        }
    }

    private void stop() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mAudioManager.abandonAudioFocusRequest(mFocusRequest);
                mMediaPlayer.stop();
            }
        }
        stopSelf();
    }

    private void fetchTrack(int id) {
        String url = Config.trackUrl + "/" + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        track = gson.fromJson(response, Track.class);

                        try {
                            mMediaPlayer.setDataSource(track.getUrl());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mMediaPlayer.prepareAsync(); // prepare async to not block main thread

                        ImageRequest imgRequest = new ImageRequest(track.getCoverUrl(),
                                new Response.Listener<Bitmap>() {
                                    @TargetApi(Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onResponse(Bitmap bitmap) {
//                                        BitmapDrawable bg = new BitmapDrawable(bitmap);
                                        track.setCover(bitmap);

                                    }
                                }, 0, 0, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.RGB_565,
                                new Response.ErrorListener() {
                                    public void onErrorResponse(VolleyError error) {
                                        //TODO: err handler
                                    }
                                });
                        App.getRequestQueue().add(imgRequest);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO: err handler
            }
        });

        // Add the request to the RequestQueue.
        App.getRequestQueue().add(stringRequest);
    }

    @Override
    /** Called when MediaPlayer is ready */
    public void onPrepared(MediaPlayer player) {
        startPlayback();
    }

}
