package online.dpulm.dplayer.ViewModels;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import online.dpulm.dplayer.Audio.Album;
import online.dpulm.dplayer.App;
import online.dpulm.dplayer.Config;

public class AlbumListViewModel extends ViewModel {
    private final String TAG = "AlbumListViewModel";
    private MutableLiveData<List<Album>> albums;
    private String mQuery = "";

    public LiveData<List<Album>> getAlbum() {
        if (albums == null) {
            albums = new MutableLiveData<>();
        }
        return albums;
    }

    public void fetchAlbums() {
        if (albums == null) {
            albums = new MutableLiveData<>();
        }
//        String url = "http://192.168.178.21:5588/search?q=" + query;
        String url = Config.albumUrl;

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                response -> {
                    long start = System.nanoTime();
                    Gson gson = new Gson();
                    List<Album> list = new ArrayList<>();

                    try {
                        for (int i = 0; i < response.length(); i++) {
                            Album album = gson.fromJson(response.get(i).toString(), Album.class);
                            if(Config.showOnDevice || !album.onDevice()) {
                                list.add(album);
                            }
                        }
                    } catch (JSONException e) {
//                            Snackbar.make(view, e.getMessage(), Snackbar.LENGTH_INDEFINITE)
//                                    .setAction("Action", null).show();
                        Log.e(TAG, e.getMessage());
                        e.printStackTrace();
                    }
                    albums.setValue(list);
                    long end = System.nanoTime();
                    long tParse = end - start;
                    Log.d(TAG, "Parsing time: " + tParse);

                }, error -> {
                    Log.e(TAG, error.getMessage());
                    //                Snackbar.make(view, error.getMessage(), Snackbar.LENGTH_LONG)
    //                        .setAction("Action", null).show();
                });

        // Add the request to the RequestQueue.
        App.getRequestQueue().add(stringRequest);
    }

    public void searchAlbums(final String query) {
        if (query != mQuery) {
            mQuery = query;
            if (albums == null) {
                albums = new MutableLiveData<>();
            }

            String url = Config.albumUrl + "/search?q=" + URLEncoder.encode(query);
//        String url = Config.albumUrl;

            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Gson gson = new Gson();
                            List<Album> list = new ArrayList<>();

                            try {
                                JSONArray rawAlbums = response;

                                for (int i = 0; i < response.length(); i++) {
                                    Album album = gson.fromJson(rawAlbums.get(i).toString(), Album.class);
                                    list.add(album);
                                }
                            } catch (JSONException e) {
                                Log.e(TAG, e.getMessage());

                                e.printStackTrace();
                            }
                            albums.setValue(list);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.getMessage());

                }
            });

            // Add the request to the RequestQueue.
            App.getRequestQueue().add(stringRequest);
        }
    }
}
