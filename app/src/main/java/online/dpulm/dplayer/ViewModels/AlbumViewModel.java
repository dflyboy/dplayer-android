package online.dpulm.dplayer.ViewModels;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import online.dpulm.dplayer.Audio.Album;
import online.dpulm.dplayer.App;
import online.dpulm.dplayer.Config;

public class AlbumViewModel extends ViewModel {
    private MutableLiveData<Album> album;
    public LiveData<Album> getAlbum() {
        if (album == null) {
            album = new MutableLiveData<Album>();
//            loadAlbum(id);
        }
        return album;
    }

    public void loadAlbum(final int id) {
        String url = Config.albumUrl + "/" + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        final Album rawAlbum = gson.fromJson(response, Album.class);
                        final String url = rawAlbum.getCoverUrl() ;

                        ImageRequest imgRequest = new ImageRequest(url,
                                new Response.Listener<Bitmap>() {
                                    @TargetApi(Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onResponse(Bitmap bitmap) {
//                                        BitmapDrawable bg = new BitmapDrawable(bitmap);
                                        rawAlbum.setCover(bitmap);
                                        if (album == null) {
                                            album = new MutableLiveData<Album>();
                                        }
                                        album.setValue(rawAlbum);
                                    }
                                }, 0, 0, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.RGB_565,
                                new Response.ErrorListener() {
                                    public void onErrorResponse(VolleyError error) {
                                        //TODO: err handler
                                    }
                                });
                        App.getRequestQueue().add(imgRequest);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO: err handler
            }
        });

        // Add the request to the RequestQueue.
        App.getRequestQueue().add(stringRequest);
    }
}
