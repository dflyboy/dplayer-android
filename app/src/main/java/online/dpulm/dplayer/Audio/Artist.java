package online.dpulm.dplayer.Audio;

public class Artist {
    private int id;
    private String name;

    public Artist(String n) {
        name = n;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
