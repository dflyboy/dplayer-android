package online.dpulm.dplayer.Audio;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import androidx.lifecycle.MutableLiveData;
import online.dpulm.dplayer.App;
import online.dpulm.dplayer.Config;

public class Track {
    public int get_id() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Album getAlbum() {
        return album;
    }

    public Artist getArtist() {
        return artist;
    }

    public int getTrackNo() {
        return trackNo;
    }

    public int getDiscNo() {
        return discNo;
    }

    public int getCoverId() {
        return coverId;
    }

    public String getUrl() {
        return Config.trackUrl + "/" + this.id + "/stream";
    }


    public Bitmap getCover() {
        return cover;
    }

    public void setCover(Bitmap cover) {
        this.cover = cover;
    }

    public String getFile() {
        return file;
    }

    public void play() {
        final String file = this.getUrl();
        Uri uri = Uri.parse(file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "audio/mpeg");
//        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        App.getAppContext().startActivity(intent);
    }

    private int id;
    private String name;
    private String file;
    private Album album;
    private Artist artist;

    private int coverId;
    private int trackNo;
    private int discNo;
    private Bitmap cover;


    Track() {}

    public String getCoverUrl() {
        return Config.coverUrl + "/" + this.coverId;
    }
}