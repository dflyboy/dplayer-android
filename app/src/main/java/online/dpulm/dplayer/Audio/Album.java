package online.dpulm.dplayer.Audio;

import android.graphics.Bitmap;

import online.dpulm.dplayer.App;
import online.dpulm.dplayer.Config;
import online.dpulm.dplayer.R;

public class Album {
    private int id;
    private String name;
    private int coverId;
    private Artist artist = null;
    private int trackN;

    public Bitmap getCover() {
        return cover;
    }

    public void setCover(Bitmap cover) {
        this.cover = cover;
    }

    private Bitmap cover;

    private int discN;
    private Track[] tracks;

    public Album(String Name, String sArtist) {
        name = Name;
        artist = new Artist(sArtist);
    }

    public int get_id() {
        return id;
    }

    public String getCoverUrl() {
        return (Config.coverUrl + "/" + this.coverId);
    }

    public Artist getArtist() {
        if(artist == null) {
            artist = new Artist(App.getAppContext().getString(R.string.artist_unknown));
        }
        return artist;
    }

    public int getTrackN() {
        return trackN;
    }

    public int getDiscN() {
        return discN;
    }

    public String getName() {
        return name;
    }

    public Track[] getTracks() {
        return tracks;
    }

    public boolean onDevice() {
        return App.hasAlbum(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Album.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Album other = (Album) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }

        if ((this.artist == null) ? (other.artist != null) : !this.artist.equals(other.artist)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
