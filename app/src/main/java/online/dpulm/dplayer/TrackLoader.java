package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import androidx.annotation.RequiresApi;

public class TrackLoader extends AsyncTask<String, String, String> {
    private static final String TAG = "TrackLoader";

    public interface CallBack {
        void setProgressPercent(int percent, String step);
        void onFinished(String path);
    }

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private DownloadManager.Query query;
    private Uri downloaded;

    private long dl;

    private TrackLoader.CallBack cb;


    @TargetApi(Build.VERSION_CODES.M)
    public TrackLoader(TrackLoader.CallBack callBack) {
        super();
        cb = callBack;
        downloadManager = App.getAppContext().getSystemService(DownloadManager.class);
    }

    @Override
    protected String doInBackground(String... args) {
        Uri uri = Uri.parse(args[0]);
        request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_MUSIC, args[1]);
        dl = downloadManager.enqueue(request);
        query = new DownloadManager.Query();

        int total = 0, bytes = 0;
        int status = 0, progress = 0;

        while (status != DownloadManager.STATUS_SUCCESSFUL) {

            Cursor cursor = downloadManager.query(query.setFilterById(dl));
            if (cursor.moveToFirst()) {

                int index = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (index > 0) {
                    status = cursor.getInt(index);
                }

                if(status == DownloadManager.STATUS_FAILED) {
                    index = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                    if (index > 0) {
                        //return on error
                        int reason = cursor.getInt(index);
                        Log.e(TAG, "Download failed: "+reason);
                        return "error " + String.valueOf(reason);
                    }
                } else if (status == DownloadManager.STATUS_RUNNING) {
                    index = cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
                    if (index > 0) {
                        bytes = cursor.getInt(index);
                    }
                    if (total < 1) {
                        index = cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
                        if (index > 0) {
                            total = cursor.getInt(index);
                        }
                    }
                    int pro = (int) ((bytes * 100.0f) / total);
                    if(progress != pro) {
                        progress = pro;
                        publishProgress(String.valueOf(pro), "Downloading");
                    }
                }

            }
        }
        downloaded = downloadManager.getUriForDownloadedFile(dl);

        return downloaded.getPath();
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
        cb.setProgressPercent(Integer.parseInt(progress[0]), progress[1]);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onPostExecute(String message) {
        cb.onFinished(message);

        //scan new files
//        App.getAppContext().getSystemService(MediaScannerConnection.class).scanFile(message, null);
    }
}
