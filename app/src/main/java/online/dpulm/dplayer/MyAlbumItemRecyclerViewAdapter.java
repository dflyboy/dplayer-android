package online.dpulm.dplayer;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import online.dpulm.dplayer.AlbumItemFragment.OnListFragmentInteractionListener;
import online.dpulm.dplayer.Audio.Album;

import java.util.List;

import static android.graphics.Color.argb;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Album} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyAlbumItemRecyclerViewAdapter extends RecyclerView.Adapter<MyAlbumItemRecyclerViewAdapter.ViewHolder> {


    private static final String TAG = "AlbumItemAdapter";
    private final List<Album> mValues;

    private final OnListFragmentInteractionListener mListener;

    private ImageLoader mImageLoader;

    public MyAlbumItemRecyclerViewAdapter(List<Album> items, OnListFragmentInteractionListener listener, ImageLoader iLoader) {
        mValues = items;
        mListener = listener;
        mImageLoader = iLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_albumitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        long start = System.nanoTime();

        holder.mItem = mValues.get(position);
        holder.mCoverView.setDefaultImageResId(R.drawable.ic_album_black_24dp);
        if (holder.mItem.getCoverUrl() != null) {
            final String url = holder.mItem.getCoverUrl() + "?w=" +  holder.mCoverView.getLayoutParams().width ;
            holder.mCoverView.setImageUrl(url, mImageLoader);
        }
        holder.mNameView.setText(holder.mItem.getName());
        holder.mArtistView.setText(holder.mItem.getArtist().getName());

        final boolean oDevice = holder.mItem.onDevice();
        holder.mCheck.setVisibility(oDevice ? View.VISIBLE : View.INVISIBLE);
//        holder.mLayout.setBackgroundColor(oDevice ? argb(18, 0, 0, 0) : argb(0, 0, 0, 0));

//        long end = System.nanoTime();
//        long tBind = end - start;
//        Log.d(TAG, "Binding time: " + tBind);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final NetworkImageView mCoverView;
        public final TextView mNameView;
        public final TextView mArtistView;
        public final ImageView mCheck;
        public final LinearLayout mLayout;
        public Album mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCoverView = (NetworkImageView) view.findViewById(R.id.cover);
            mNameView = (TextView) view.findViewById(R.id.name);
            mArtistView = (TextView) view.findViewById(R.id.text_artist);
            mCheck = view.findViewById(R.id.checkMark);
            mLayout = view.findViewById(R.id.i_container);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
