package online.dpulm.dplayer;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import static androidx.core.content.ContextCompat.getSystemService;

public class Config {
    public static final boolean showOnDevice = false;
    public static final String wifiUrl = "http://192.168.178.54:5588/";
    public static final String broadUrl = "http://dflyboy420.dynu.net:5588/";

    public static final String baseUrl;
//    public static final String baseUrl = "http://192.168.178.21:5588/";

    public static final String albumUrl, coverUrl, trackUrl, indexUrl;

    static {

        ConnectivityManager connMgr = (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = networkInfo.isConnected();

        baseUrl = isWifiConn ? wifiUrl : broadUrl;

        albumUrl = baseUrl + "albums";
        coverUrl = baseUrl + "cover";
        trackUrl = baseUrl + "tracks";
        indexUrl = baseUrl + "index";

        Log.d("CONFIG STATIC", baseUrl);
    }


}
