package online.dpulm.dplayer;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import online.dpulm.dplayer.Audio.Album;
import online.dpulm.dplayer.ViewModels.AlbumListViewModel;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class AlbumItemFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static String TAG = "AlbumItemFragment";
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_SEARCH_QUERY = "search-query";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private List<Album> albums;
    private MyAlbumItemRecyclerViewAdapter mAdapter;
//    private String query = "init";

    //    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AlbumItemFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static AlbumItemFragment newInstance(int columnCount, String search) {
        AlbumItemFragment fragment = new AlbumItemFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
//        args.putString(ARG_SEARCH_QUERY, search);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//            query = getArguments().getString(ARG_SEARCH_QUERY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albumitem_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();

            mImageLoader = App.getImageLoader();

            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), ((LinearLayoutManager) layoutManager).getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            //get search query
            AlbumListActivity albumListActivity = (AlbumListActivity) getActivity();
            String query = albumListActivity.getQuery();
//            recyclerView.setAdapter(new MyAlbumItemRecyclerViewAdapter(DummyContent.ITEMS, mListener));
            AlbumListViewModel model = ViewModelProviders.of(getActivity()).get(AlbumListViewModel.class);


            if(query == null) {
                model.fetchAlbums();
            } else {
                model.searchAlbums(query);
            }

            model.getAlbum().observe(this, item -> {
                long start = System.nanoTime();

                albums = item;
                mAdapter = new MyAlbumItemRecyclerViewAdapter(albums, mListener, mImageLoader);
                recyclerView.setAdapter(mAdapter);

                albumListActivity.hideProgress();

                long end = System.nanoTime();
                long tParse = end - start;
                Log.d(TAG, "Rendering time: " + tParse);
            });

        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        mListener = (OnListFragmentInteractionListener) context;
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Album item);
    }
}
