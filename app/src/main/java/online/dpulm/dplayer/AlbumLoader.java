package online.dpulm.dplayer;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class AlbumLoader extends AsyncTask<String, String, String> {
    private static final String TAG = "AlbumLoader";

    public interface CallBack {
        void setProgressPercent(int percent, String step);
        void onFinished(String path);
    }

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private DownloadManager.Query query;
    private Uri downloaded;

    private long dl;

    private CallBack cb;
    private ArrayList<String> files;

    @TargetApi(Build.VERSION_CODES.M)
    public AlbumLoader(CallBack callBack) {
        super();
        cb = callBack;
        downloadManager = App.getAppContext().getSystemService(DownloadManager.class);
        files = new ArrayList<>();
    }

    protected void unzip() {
        try {
            publishProgress(String.valueOf(100), "Unzipping");

            ParcelFileDescriptor file = downloadManager.openDownloadedFile(dl);
            ParcelFileDescriptor.AutoCloseInputStream is = new ParcelFileDescriptor.AutoCloseInputStream(file);

            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);

            InputStream in;

            String mimeType = App.getAppContext().getContentResolver().getType(downloaded);
            if(mimeType.contains("gzip")) {
                in =  new GzipCompressorInputStream(new BufferedInputStream(is));
            } else {
                in = new BufferedInputStream(is);
            }

            TarArchiveInputStream fin = new TarArchiveInputStream(in);
            TarArchiveEntry entry;
            while ((entry = fin.getNextTarEntry()) != null) {
                String fileName = entry.getName();
                File newFile = new File(folder, fileName);

                if (entry.isDirectory()) {
//                    File fmd = new File(folder, fileName);
                    newFile.mkdirs();
                    continue;
                }

                files.add(newFile.getAbsolutePath());
                Log.d(TAG, "Unzipping " + files.get(files.size()-1));
                File parent = newFile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
//                FileOutputStream fos = App.getAppContext().openFileOutput(newFile.getPath(), 0);

                FileOutputStream fos = new FileOutputStream(newFile);
                IOUtils.copy(fin, fos);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        Uri uri = Uri.parse(urls[0]);
        request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
//        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "dplayer/download.tar");
        dl = downloadManager.enqueue(request);
        query = new DownloadManager.Query();

        int total = 0, bytes = 0;
        int status = 0, progress = 0;

        while (status != DownloadManager.STATUS_SUCCESSFUL) {
            Cursor cursor = downloadManager.query(query.setFilterById(dl));
            if (cursor.moveToFirst()) {

                int index = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (index > 0) {
                    status = cursor.getInt(index);
                }

                if (status == DownloadManager.STATUS_RUNNING) {
                    index = cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
                    if (index > 0) {
                        bytes = cursor.getInt(index);
                    }
                    if (total < 1) {
                        index = cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
                        if (index > 0) {
                            total = cursor.getInt(index);
                        }
                    }
                    int pro = (int) ((bytes * 100.0f) / total);
                    if(progress != pro) {
                        progress = pro;
                        publishProgress(String.valueOf(pro), "Downloading");
                    }
                }

            }
        }
        downloaded = downloadManager.getUriForDownloadedFile(dl);
        unzip();

        return downloaded.getPath();
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
        cb.setProgressPercent(Integer.parseInt(progress[0]), progress[1]);
    }


    @Override
    protected void onPostExecute(String message) {
        StringBuilder sb = new StringBuilder();
        for (String s : files) {
            sb.append(s);
            sb.append("\n");
        }
        cb.onFinished(sb.toString());

    //scan new files
        MediaScannerConnection.scanFile(App.getAppContext(), files.toArray(new String[files.size()]), null, new MediaScannerConnection.OnScanCompletedListener() {
            @Override
            public void onScanCompleted(String path, Uri uri) {
                Log.d(TAG, "Scan complete");
            }
        });
    }

}
