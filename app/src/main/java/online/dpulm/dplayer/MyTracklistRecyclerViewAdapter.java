package online.dpulm.dplayer;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import online.dpulm.dplayer.Audio.Track;
import online.dpulm.dplayer.TracklistFragment.OnListFragmentInteractionListener;

import java.util.List;

import static online.dpulm.dplayer.AlbumLoadService.ACTION_LOAD;

public class MyTracklistRecyclerViewAdapter extends RecyclerView.Adapter<MyTracklistRecyclerViewAdapter.ViewHolder> {

    private final List<Track> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyTracklistRecyclerViewAdapter(List<Track> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tracklist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        if(mValues.get(position).getTrackNo() > 0) {
            holder.mIdView.setText(String.valueOf(holder.mItem.getTrackNo()));
        }
        holder.mContentView.setText(holder.mItem.getName());

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });

        holder.dlButton.setOnClickListener(v -> {
            Intent intent = new Intent(App.getAppContext(), TrackLoaderService.class);
            intent.putExtra(TrackLoaderService.TRACK_ID, holder.mItem.get_id());
//            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            App.getAppContext().startService(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageButton dlButton;
        public Track mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            dlButton = view.findViewById(R.id.dlButton);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
